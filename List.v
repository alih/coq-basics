(* List.v - basic facts about Coq lists in addition to Coq's library
 * (C) 2015  Ilya Mezhirov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *)

(*
 * The "basic" operations on lists include:
 *
 *   :: (cons)
 *   ++ (app)
 *   length
 *   head
 *   tail
 *   In
 *   nth
 *   nth_error (should I rename it to maybe_nth?
 *   ith (defined here)
 *   map
 *   concat
 *
 * Everything else goes elsewhere.
 *)

Require Import Coq.Lists.List.
Require Import Arith.

Require Import Basics.DepTypes.

(***********************************************************************)

Section Lists.

Context {A: Type}.

(* This is a short section that defines hd and tl. *)

(* This is a simple redefinition into my naming convention. *)
Definition maybe_head (l: list A)
: option A
:= hd_error l.

Definition maybe_tail (l: list A)
: option (list A) 
:= match l with
   | h :: t => Some t
   | nil => None
   end.

Lemma nonempty_head (l: list A): l <> nil -> maybe_head l <> None.
intro. destruct l. tauto. simpl. unfold value. discriminate. Qed.

Lemma nonempty_tail (l: list A): l <> nil -> maybe_tail l <> None.
intro. destruct l. tauto. simpl. discriminate. Qed.

Definition nonempty_list A := { l: list A | l <> nil }.

Definition head
: nonempty_list A -> A
:= restrict maybe_head nonempty_head.

Definition tail
: nonempty_list A -> list A
:= restrict maybe_tail nonempty_tail.

Lemma head_tail (l: nonempty_list A):
  head l :: tail l = unspecify l.
destruct l. simpl. destruct x. exfalso. apply n. trivial.
assert(head_ok: head (exist (fun l : list A => l <> nil) (a :: x) n) = a).
unfold head. somify. simpl. trivial.
assert(tail_ok: tail (exist (fun l : list A => l <> nil) (a :: x) n) = x).
unfold tail. somify. simpl. trivial.
rewrite head_ok. rewrite tail_ok.
trivial.
Qed.

Lemma to_head_and_tail (l: list A) (nonempty: l <> nil):
  l = head (specify l nonempty) :: tail (specify l nonempty).
rewrite (head_tail (specify l nonempty)).
simpl. trivial.
Qed.

(** A list of length 0 is empty. *)
Lemma list_empty_if_length_0:
  forall (l: list A),
    length l = 0 -> l = nil.
induction l.
simpl. trivial.
simpl. intro. discriminate H. Qed.

Lemma if_nonempty_then_positive_length (l: list A):
  l <> nil -> length l > 0.
induction l. tauto. simpl. intro. auto with arith. Qed.

End Lists.

(***********************************************************************)

Section Facts.

Context {A: Type}.

(** Alternative definition of In through list construction. *)
Theorem in_decomp (x: A) (l: list A):
    In x l <-> exists a, exists b, l = a ++ x :: b.
Proof.
induction l.
{
  simpl. split.
  {
    tauto.
  }
  intro.
  destruct H.
  destruct H.
  assert(length (nil: list A) = length (x0 ++ x :: x1)).
  {
    rewrite H. trivial.
  }
  rewrite app_length in H0. simpl in H0.
  symmetry in H0.
  remember (length x0) as n0.
  remember (length x1) as n1.
  assert(n0 + S n1 > 0).
  assert(S n1 > 0).
  auto with arith.
  rewrite plus_comm.
  auto with arith.
  rewrite H0 in H1.
  generalize H1.
  apply lt_irrefl.
}
(* step *)
simpl.
split.
{
 intro x_in_al. 
 destruct x_in_al. 
 {
   rewrite H.
   exists nil. exists l. simpl. trivial.
 }
 rewrite IHl in H. destruct H. exists (a :: x0).
  simpl. destruct H. exists x1. rewrite H. trivial.
}
(* right *)
intro E. destruct E as (prefix, Eb). destruct Eb as (b, Eq).
destruct prefix.
{
  simpl in Eq.
  injection Eq.
  tauto.
}
right.
injection Eq.
intro H. intro aa0.
apply IHl. exists prefix. exists b.
assumption.
Qed.

Lemma in_unspecify {P: A -> Prop}
                   (proof_irrel: forall (Z: Prop) (x y: Z), x = y)
                   (a: {x: A | P x})
                   (l: list {x: A | P x}):
  In a l <-> In (unspecify a) (map unspecify l).
Proof.
induction l.
{
  simpl. tauto.
}
simpl.
{
  split.
  {
    intro H. destruct H.
    { rewrite H. left. trivial. }
    right. tauto.
  }
  intro H. destruct H.
  {
    (* proof-irrel time *)
    left.
    apply (unspecify_eq proof_irrel). exact H.
  }
  right.
  apply IHl.
  exact H.
}
Qed.


(** Two items of a list that are not in initial segments of each other are one.
   In other words, two decompositions of the same list:
      a  x  b
   and 
      a' x' b'
   are the same decomposition if x' is not in a and x is not in a'.
 *)
Lemma list_eq_by_mutual_bound:
  forall a b a' b': list A, forall x x': A,
    a ++ x :: b = a' ++ x' :: b'
   -> ~In x a'
   -> ~In x' a
     -> a = a' /\ x = x' /\ b = b'.
Proof.
induction a.
{
  simpl. destruct a'.
  { 
    simpl. intros. split. trivial. split.
    {
      injection H. intros. assumption.
    }
    injection H. intros. assumption.
  }
  simpl. intros. assert(a = x).
  assert(hd a (x :: b) = hd a (a :: a' ++ x' :: b')).
  {
    rewrite H. trivial.
  } 
  simpl in H2. symmetry. assumption.
  tauto.
}
simpl. intros. destruct a'. simpl in H. injection H. intros. tauto.
rewrite<- app_comm_cons in H.
injection H. intros. rewrite_all<- H3.
assert(a0 = a' /\ x = x' /\ b = b').
{
  apply IHa.
  assumption.
  { simpl in H0.  tauto. }
  tauto.
}
destruct H4. rewrite H4. split. trivial. assumption.
Qed.

End Facts.


(************************************   ith   **********************************)

Section Elts.

(** A property that holds everywhere in the list holds for its particular item.
  This version checks for the index of the item to be in range.
 *)
Theorem nth_prop_in_range {A: Type} (a: list A) (i: nat) (P: A->Prop)
                          (i_ok: i < length a) (a_all: Forall P a) (d: A):
  P (nth i a d).
rewrite Forall_forall in a_all.
apply a_all.
apply nth_In. assumption.
Qed.

(** A property that holds everywhere in the list holds for its particular item.
  This version requires the default of nth to comply with the property also.
 *)
Theorem nth_prop_with_default {A: Type} (a: list A) (i: nat) (P: A->Prop) (d: A)
                              (d_ok: P d) (a_all: Forall P a):
    P (nth i a d).
assert({In (nth i a d) a} + {nth i a d = d}).
apply nth_in_or_default.
case H. rewrite Forall_forall in a_all. apply a_all. intro D. rewrite D. exact d_ok. Qed.

Lemma nth_error_throws_on_length {A: Type}:
  forall l: list A,
         nth_error l (length l) = None.
intros.
induction l. simpl. trivial. simpl. assumption. Qed.

Lemma nth_error_throws_S {A: Type}:
  forall i: nat,
    forall l: list A, 
         nth_error l i = None -> nth_error l (S i) = None.
induction i. induction l. simpl. trivial. simpl. intro. discriminate H.
induction l. simpl. trivial. intro. simpl in H. 
apply IHi in H. simpl. induction l. trivial. simpl in H. assumption. Qed.

Lemma nth_error_throws_plus {A: Type}:
  forall i: nat,
    forall l: list A, 
       nth_error l (length l + i) = None.
induction i. intro. rewrite plus_0_r. apply nth_error_throws_on_length.
intro. rewrite <- plus_n_Sm. apply nth_error_throws_S. apply IHi. Qed.

Lemma nth_error_throws {A: Type}:
  forall i: nat,
    forall l: list A, 
       i >= length l -> nth_error l i = None.
intros.
assert(i = length l + (i - length l)).
{
  symmetry.
  apply le_plus_minus_r.
  auto with arith.
}
rewrite H0.
apply nth_error_throws_plus. Qed.

Lemma nth_error_nth {A: Type} (l: list A) (i: nat):
       forall d: A,
         i < length l ->
           nth_error l i = Some (nth i l d).
generalize l i.
induction l0. intros. simpl in H. assert (~(i0 < 0)). auto with arith. tauto.
intros. simpl in H. induction i0. simpl. trivial. simpl.
apply IHl0. auto with arith. Qed. 

Lemma nth_error_in {A: Type} (l: list A) (i: nat) (a: A):
  nth_error l i = Some a -> In a l.
assert(i >= length l \/ i < length l).
apply le_or_lt.
destruct H.
intro.
assert(nth_error l i = None).
apply(nth_error_throws). assumption. rewrite H1 in H0. discriminate H0.
rewrite nth_error_nth with (d:=a). 
intro. inversion H0. 
apply nth_In. assumption. assumption. Qed.

Lemma nth_error_works {A: Type}:
  forall l: list A,
    forall i: nat,
       i < length l ->
         nth_error l i <> None.
intros.
induction l. simpl in H. assert (~(i < 0)). auto with arith. tauto.
rewrite nth_error_nth with (d := a). discriminate. assumption. Qed.

Lemma nth_error_app_rewrite {A: Type} (a b: list A) (x: A):
  nth_error (a ++ x :: b) (length a) = Some x.
Proof.
induction a.
simpl. trivial. simpl. assumption. 
Qed.

Definition ith {A: Type}
               (l: list A)
: {x: nat | x < length l } -> A
:= restrict (nth_error l) (nth_error_works l).

Lemma ith_irrel {A: Type}
                (l: list A)
                (i: nat) (p1 p2: i < length l):
  ith l (specify i p1) = ith l (specify i p2).
Proof.
unfold ith. unfold restrict. simpl. apply unsome_irrel. 
Qed.

Lemma ith_p {A} (l: list A) (i: nat) (P: A -> Prop)
    (i_ok: i < length l)
    (x_ok: match nth_error l i with 
           | Some x => P x
           | None => True
           end)
: P (ith l (specify i i_ok)).
Proof.
unfold ith. apply restrict_p1. trivial. 
Qed.

Lemma ith_nil {A: Type}
  (l: list A) (i: {x: nat | x < length l}):
     l = nil -> False.
Proof.
intro.
rewrite H in i. simpl in i.
assert (unspecify i < 0).
exact (certificate i).
assert(~(unspecify i < 0)).
auto with arith.
tauto.
Qed.


Theorem ith_nth {A: Type} (l: list A)
                (i: {x: nat | x < length l}) (d: A):
  ith l i = nth (unspecify i) l d.
Proof.
assert(unspecify i < length l).
apply (certificate i).
unfold ith. unfold restrict.
somify. 
apply nth_error_nth. assumption.
Qed.

Theorem ith_nth_error  {A: Type} (l: list A)
(i: {x: nat | x < length l}):
  Some (ith l i) = nth_error l (unspecify i).
Proof.
unfold ith. unfold restrict. rewrite some_unsome. trivial.
Qed.

Theorem nth_error_ith {A: Type} (l: list A)
                      (i: nat) (ok: i < length l):
  nth_error l i = Some (ith l (specify i ok)).
Proof.
unfold ith. unfold restrict. rewrite some_unsome. trivial.
Qed.

Lemma ith_nth_self {A: Type} (l: list A) (i: {x: nat | x < length l}):
  ith l i = nth (unspecify i) l (ith l i).
Proof.
apply ith_nth.
Qed.

Theorem ith_In {A: Type} (l: list A)
  (i: {x | x < length l}):
    In (ith l i) l.
Proof.
assert(C := certificate i).
rewrite ith_nth_self.
apply nth_In. assumption. 
Qed.

(* This lemma fixes In_nth from the Coq library,
 * which makes n a function of d.
 *)
Lemma In_nth' {A} (l: list A) (x: A): In x l ->
    exists n, n < length l /\ forall d: A, nth n l d = x.
Proof.
induction l.
{
  intro. exfalso. auto.
}
intro H.
simpl in H.
destruct H.
{
  exists 0.
  simpl.
  auto with arith.
}
assert (E := IHl H).
clear IHl H.
destruct E as (n, (L, E)).
exists (S n).
simpl.
split. {  auto with arith. }
assumption.
Qed.

Theorem In_ith {A: Type} (l: list A) (x: A):
  In x l -> exists i:  {x | x < length l}, 
               x = ith l i.
Proof.
intro H.
assert(N := In_nth_error l x H).
destruct N as (n, N).
assert(in_range: n < length l).
{
  apply nth_error_Some.
  rewrite N.
  discriminate.
}
exists (specify n in_range).
remember (specify n in_range) as i.
rewrite nth_error_ith with (ok := in_range) in N.
injection N.
intro X.
rewrite<- X.
rewrite Heqi.
apply ith_irrel.
Qed.

Lemma last_nth {A} 
               (l: list A):
  forall (i: nat)
         (i_ok : i < length l)
         (dummy: A),
    Some (last (firstn (S i) l) dummy)
     = 
    nth_error l i.
Proof.
induction l.
{
  intros. simpl in i_ok. apply PeanoNat.Nat.nlt_0_r in i_ok.
  contradiction.
}
intros.
simpl.
destruct i.
{
  simpl. trivial.
}
simpl. rewrite<- IHl with (dummy:=dummy). destruct l.
{
  simpl in i_ok. assert(Bad: i < 0). auto with arith. 
  apply PeanoNat.Nat.nlt_0_r in Bad. contradiction.
}
simpl. trivial.
simpl in i_ok. auto with arith.
Qed. 

Lemma last_ith {A} 
               (l: list A)
               (i: nat)
               (i_ok : i < length l)
               (dummy: A):
    last (firstn (S i) l) dummy = ith l (specify i i_ok).
Proof.
assert (S: Some (last (firstn (S i) l) dummy) = Some (ith l (specify i i_ok))).
rewrite last_nth. rewrite ith_nth_error. simpl. trivial.
assumption. injection S. simpl. trivial.
Qed.

Lemma ith_singleton {A}
                    (a: A):
  forall i,
    ith (a :: nil) i = a.
Proof.
intro i. destruct i as (n, C).
simpl in C.
assert (n = 0).
{
  induction n. trivial.
  assert(n < 0). auto with arith.
  assert(~(n < 0)). auto with arith.
  contradiction.
}
simpl.
rewrite ith_nth_self.
simpl. destruct n. trivial. exfalso. discriminate H.
Qed.

Lemma map_ith_helper {A B: Type} {f: A -> B} (l: list A)
                     (x: nat) : x < length (map f l) -> x < length l.
Proof.
rewrite map_length. trivial.
Qed.

Lemma map_ith {A B: Type} {f: A -> B} (l: list A):
  forall i,
    ith (map f l) i = f (ith l ((respecify (map_ith_helper l)) i)).
Proof.
intro i.
remember (ith (map f l) i) as b.
remember (ith l (respecify (map_ith_helper l) i)) as a.
assert(defa:=Heqa).
rewrite ith_nth_self in Heqa.
rewrite<- defa in Heqa.
simpl in Heqa.
rewrite ith_nth with (d:=f a) in Heqb.
rewrite Heqa. rewrite Heqb. apply map_nth. 
Qed.

Lemma ith_map_helper {A B: Type} {f: A -> B} (l: list A)
                     (x: nat):
    x < length l -> x < length (map f l).
Proof.
rewrite<- map_length with (f:=f). trivial. 
Qed.

Lemma ith_map {A B: Type} {f: A -> B} (l: list A):
  forall i,
    f (ith l i) = ith (map f l) (respecify (ith_map_helper l) i).
Proof.
intro. rewrite map_ith.
remember (respecify (map_ith_helper l) (respecify (ith_map_helper l) i)) as j.
assert(ith l i = ith l j).
unfold ith. unfold restrict. simpl. rewrite Heqj.  simpl. apply unsome_irrel. rewrite H. trivial.
Qed.

Lemma map_unspecify_specify {A} (l: list A) (P Q: A -> Prop):
  forall p q,
    let lp: list {x: A | P x} := map (fun x => specify x (p x)) l in
    let lq: list {x: A | Q x} := map (fun x => specify x (q x)) l in
  map unspecify lp = map unspecify lq.
induction l. simpl. trivial.
simpl. intros.
rewrite (IHl p q). trivial.
Qed.

End Elts.

(************************************   map   **********************************)

Section Map.
  Context {A B : Type}.
  Variable f : A -> B.

(** A variation on map_nth that checks that the index is in range. *)
Theorem map_nth_in_range:
  forall l d d' n,
    n < length l ->
      nth n (map f l) d = f (nth n l d').
induction l. simpl map. intros. simpl in H. exfalso. assert(~(n < 0)). auto with arith. tauto.
intros. simpl map. destruct n. simpl. trivial. simpl.
apply IHl. simpl in H. auto with arith. Qed.

End Map.

Definition concat {A: Type} (l: list (list A))
: list A
:= fold_right (fun (a b: list A) => app a b) nil l.

Lemma concat_in {A: Type} (l: list (list A)) (m: list A) (x: A):
  In x m -> In m l -> In x (concat l).
Proof.
intros XM ML.
induction l. { simpl. simpl in ML. assumption. }
simpl.
simpl in ML.
destruct ML as [Here|There].
{
  rewrite Here.
  rewrite in_app_iff.
  left. exact XM.
}
rewrite in_app_iff.
right.
exact (IHl There).
Qed.

(* vim: set ft=coq: *)
