(* ListIndex.v - simple linear searching in lists
 * (C) 2015  Ilya Mezhirov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *)

Require Import Arith.
Require Import Lists.List.
Require Import Basics.DepTypes.
Require Import Basics.List.
Require Import Basics.ListCut.
Require Import Basics.MinMax.
Require Import Omega.

(***********************************************************************)

Section Index.

Variable A: Type.
Hypothesis eq_dec : forall x y : A, {x = y}+{x <> y}.

Lemma if_eq:
  forall Q: Type,
  forall x:A,
  forall a b:Q,
    (if eq_dec x x then a else b) = a.
intros.
destruct (eq_dec x x). trivial. tauto. Qed. 

Lemma if_not_eq:
  forall Q: Type,
  forall x y:A,
  forall a b:Q,
    x <> y -> (if eq_dec x y then a else b) = b.
intros.
destruct (eq_dec x y). contradiction. trivial. Qed.

Fixpoint inb (a: A) (l: list A): bool :=
  match l with
  | nil => false
  | h :: t => if eq_dec h a then true else
                inb a t
  end.

Lemma inb_prop (a: A) (l: list A):
  inb a l = true <-> In a l.
Proof.
induction l.
{ simpl. intuition. }
simpl. destruct eq_dec.
tauto. rewrite IHl. tauto. Qed.

Fixpoint index a (l: list A): option nat :=
  match l with
  | nil => None
  | h :: t => if eq_dec h a then Some 0 else 
                match index a t with
                | Some i => Some (S i)
                | None => None
                end
  end.

Lemma index_in:
    forall k l,
        In k l -> exists n: nat, index k l = Some n.
induction l.
intros. unfold In. contradiction.
intros. unfold In in H. destruct H. exists 0. simpl. rewrite H.
destruct eq_dec. trivial.
tauto.
apply IHl in H. destruct H.
exists (if eq_dec a k then 0 else S x).
unfold index. destruct eq_dec. trivial. fold index. rewrite H. trivial. Qed.

Lemma index_works:
    forall k l,
        In k l -> index k l <> None.
intros.
apply index_in in H.
destruct H. rewrite H. discriminate. Qed.

Lemma index_works_b (l: list A) (k: A):
        inb k l = true -> index k l <> None.
rewrite inb_prop. apply index_works. Qed.

Definition item (l: list A): Type
:= { a: A | inb a l = true}.

Definition itemindex (l: list A)
: item l -> nat
:= restrict (fun x => index x l) (index_works_b l).

Lemma index_not_in:
  forall x a,
    ~In x a -> index x a = None.
induction a. simpl. trivial.
simpl. intro.
destruct (eq_dec a x).
destruct H. left. assumption.
assert(~In x a0).
tauto.
apply IHa in H0.
rewrite H0. trivial. Qed.

Lemma index_bound:
    forall a l,
        forall n, index a l = Some n -> n < length l.
induction l.
intros. unfold index in H. discriminate H.
intros. unfold index in H. destruct eq_dec.
injection H. intros. rewrite<- H0. unfold length. auto with *.
fold index in H.
unfold length.
destruct (index a l).
injection H. intros. clear H. rewrite<- H0.
assert(n1 < length l).
apply IHl. trivial.
auto with *.
discriminate H. Qed.

Lemma itemindex_bound:
  forall (l: list A) a,
    itemindex l a < length l.
unfold itemindex.
assert(bound := index_bound).
intros.
assert(index (unspecify a) l = Some (restrict (fun x : A => index x l)
  (index_works_b l) a)).
{
  rewrite some_restrict. trivial.
}
apply bound in H.
exact H.
Qed.

Lemma index_firstn:
  forall a x,
    ~ In x (match index x a with
            | Some n => firstn n a
            | None => a
            end).
induction a.
simpl. unfold not. trivial.
simpl. intro. destruct (eq_dec a x).
simpl. unfold not. trivial.
assert (~In x match index x a0 with
             | Some n => firstn n a0
             | None => a0
             end).
apply IHa.
remember (index x a0) as i.
destruct i.
simpl.
tauto.
simpl.
tauto. Qed.

Lemma index_firstn_some:
  forall a x n,
    index x a = Some n ->
    ~ In x (firstn n a).
intros.
assert(~ In x (match index x a with
            | Some n => firstn n a
            | None => a
            end)). apply index_firstn. rewrite H in H0. assumption. Qed.

Lemma index_none:
  forall a x,
    index x a = None -> ~ In x a.
intros.
assert(~ In x (match index x a with
            | Some n => firstn n a
            | None => a
            end)). apply index_firstn. rewrite H in H0. assumption. Qed.

Lemma index_in_bound:
  forall a n d,
    n < length a ->
      exists k, Some k = index (nth n a d) a /\ k <= n.
induction a.
simpl.
intros.
apply Lt.lt_n_O in H. contradiction.
intros.
simpl in H.
assert(n > 0 -> n = S (pred n)).
apply Lt.S_pred.
assert(n <= 0 \/ 0 < n).
apply Lt.le_or_lt.
destruct H1.
assert(n = 0).
auto with arith.
rewrite H2.
simpl. rewrite if_eq. exists 0. split. trivial. trivial.
apply H0 in H1.
clear H0.
rewrite H1.
simpl.
specialize IHa with (n := pred n) (d := d).
rewrite H1 in H.
assert(pred n < length a0).
auto with arith.
apply IHa in H0.
destruct H0.
exists (if eq_dec a (nth (pred n) a0 d) then 0 else S x).
destruct H0.
rewrite<- H0.
split.
destruct (eq_dec a (nth (pred n) a0 d)). trivial. trivial.
destruct (eq_dec a (nth (pred n) a0 d)). auto with arith. auto with arith. Qed.


Lemma index_nth:
  forall a x,
    match index x a with
    | Some n => forall d, nth n a d = x
    | None => ~In x a
    end.
induction a.
simpl. unfold not. trivial.
intros. simpl. destruct (eq_dec a x). tauto.
assert(match index x a0 with
       | Some n => forall d : A, nth n a0 d = x
       | None => ~ In x a0
       end). apply IHa.
remember (index x a0) as i.
destruct i. assumption.
tauto. Qed.

Lemma index_nth_some:
  forall a x n,
    index x a = Some n -> forall d, nth n a d = x.
intros.
assert(match index x a with
    | Some n => forall d, nth n a d = x
    | None => ~In x a
    end). apply(index_nth). rewrite H in H0. apply H0. Qed.

Lemma index_from_firstn:
  forall n a d,
    n < length a ->
    ~ In (nth n a d) (firstn n a) ->
      index (nth n a d) a = Some n.
intros.
remember (nth n a d) as x.
assert(~ In x (match index x a with
            | Some n => firstn n a
            | None => a
            end)).
apply index_firstn.
assert(In x a).
rewrite Heqx.
apply nth_In. assumption.
remember (index x a) as i.
destruct i.
Focus 2.
contradiction.
assert(forall d, nth n0 a d = x).
apply index_nth_some. symmetry. assumption.
assert((firstn n a) ++ (skipn n a) = a).
apply(firstn_skipn).
assert((firstn n0 a) ++ (skipn n0 a) = a).
apply(firstn_skipn).
assert(skipn n a = (nth n a d) :: (skipn (S n) a)).
apply skipn_decompose. assumption.
assert(skipn n0 a = (nth n0 a d) :: (skipn (S n0) a)).
apply skipn_decompose. apply index_bound with (a:=x). symmetry. assumption.
rewrite H6 in H4. rewrite H7 in H5.
assert(firstn n a = firstn n0 a /\ nth n a d = nth n0 a d /\ skipn (S n) a = skipn (S n0) a).
apply(list_eq_by_mutual_bound).
rewrite H4. rewrite H5. trivial. rewrite<- Heqx. assumption. rewrite H3. assumption.
destruct H8.
assert(length (firstn n a) = length(firstn n0 a)).
rewrite H8. trivial. rewrite_all firstn_length. 
assert(min n (length a) = n).
apply(min_l). auto with arith. rewrite H11 in H10.
assert(min n0 (length a) = n0). apply(min_l). 
assert(n0 < length a).
apply index_bound with (a:=x). symmetry. assumption. auto with arith.
rewrite H12 in H10. rewrite H10. trivial. Qed.


Lemma index_lower_bound:
  forall x a n i,
    ~ In x (firstn n a)
    -> index x a = Some i 
      -> i >= n.
intros.
assert(~ In x (match index x a with
            | Some k => firstn k a
            | None => a
            end)).
apply index_firstn.
rewrite H0 in H1.
assert(forall d, nth i a d = x).
apply index_nth_some. assumption.
assert(forall k, k > i -> In x (firstn k a)).
intros.
assert(forall d, nth i (firstn k a) d = nth i a d).
intros.
symmetry.
rewrite firstn_nth with (n := k). trivial. apply H3.
rewrite<- H2 with (d:=x).
rewrite firstn_nth with (n:=k).
apply nth_In.
rewrite firstn_length.
kick_min.
omega.
apply index_bound with (a:=x). exact H0. apply H3.
assert(i < n \/ i >= n).
assert(i >= n \/ i < n).
apply Lt.le_or_lt.
tauto.
destruct H4.
apply H3 in H4.
contradiction.
assumption. Qed.

End Index.

(* vim: set ft=coq: *)
