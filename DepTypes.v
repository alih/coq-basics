(* DepTypes.v - dependent types in a (hopefully) clean, simple style
 * (C) 2015-2016  Ilya Mezhirov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *)

Require Setoid.

(** Construct a (value, proof) pair of type {x|P x} *)
Definition specify {A: Type} {P: A -> Prop}
                   (a: A) (cert: P a)
: {x: A | P x} 
:= exist (fun x => P x) a cert.

(** Extract the value from a value-proof pair *)
Definition unspecify {A: Type} {P: A -> Prop}
                     (a: {x: A | P x})
: A               
:= proj1_sig a.

(** Extract the proof from a value-proof pair *)
Definition certificate {A: Type} {P: A -> Prop}
                       (a: {x: A | P x})
: P (unspecify a)
:= proj2_sig a.

(**
 * Widen the set that a value belongs to.
 * The order of arguments is significant:
 * respecify lifts a 
 *   P x -> Q x 
 * to
 *   { x: A | P x } -> { x: A | Q x }
 *)
Definition respecify {A: Type} {P: A -> Prop} {Q: A -> Prop}
                     (move: forall x, P x -> Q x)
                     (a: { x: A | P x })
                     
: { x: A | Q x } 
:= specify (unspecify a) (move (unspecify a) (certificate a)).

(**
 * Make a rewrite in the specification.
 * (hey, I want that as a tactic!)
 *)
Program Definition rewrite_spec {A B: Type} {b: B} {c: B}
                         {P: B -> A -> Prop}
                         (a: { x: A | P b x })
                         (move: b = c)
: { x: A | P c x }
:= respecify _ a.

(**
 * Make a rewrite in the value.
 *)
Program Definition copy_spec {A: Type} {P: A -> Prop}
  (a: { x: A | P x }) (b: A) (move: b = unspecify a)
: { x: A | P x }
:= specify b (_ move (unspecify a) (certificate a)).

Lemma specify_eq:
  (forall (A: Prop) (p q: A), p = q) ->
     forall (A: Type) (P: A -> Prop)
            (x y: A)
            (px: P x) (py: P y)
            (E: x = y),
       specify x px = specify y py.
Proof.
intro proof_irrel.
intros.
destruct E. 
assert(R: px = py). { apply proof_irrel. }
rewrite R. trivial.
Qed.

Lemma unspecify_eq:
  (forall (A: Prop) (p q: A), p = q) ->
     forall (A: Type) (P: A -> Prop)
            (a b: {x: A | P x})
            (E: unspecify a = unspecify b),
        a = b.
Proof.
intro proof_irrel. intros.
destruct a as (x, p). destruct b as (y, q). simpl in E.
apply (specify_eq proof_irrel). assumption.
Qed.

(********************************************************************)

(**
 * Unbox the option when it's guaranteed that None is impossible.
 *)
Program Definition unsome {A: Type}
                          (o: option A) (nonempty: o <> None)
: A 
:= match o with
   | Some answer => answer
   | None => _
   end.

Lemma unsome_some {A: Type} (x: A):
  forall c, unsome (Some x) c = x.
unfold unsome. trivial. Qed.

Lemma some_unsome {A: Type} (o: option A):
  forall c, Some (unsome o c) = o.
intro.
destruct o. rewrite unsome_some. trivial.
exfalso. tauto. Qed.


Lemma some_exists {A: Type} (o: option A):
  o <> None -> exists x, o = Some x.
intro. destruct o. exists a. trivial. tauto. Qed.


Lemma somify_helper {T: Type} {a b: T}:
  Some a = Some b -> a = b.
intro. inversion H. trivial. Qed.

(**
 * Convert a partial map to a total map from a subset.
 *)
Definition restrict {A B: Type} {P: A -> Prop}
                    (f: A -> option B)
                    (guarantee: forall x: A, P x -> f x <> None)
: {x: A | P x} -> B
:= fun a => let arg: A := unspecify a in
            let c1: P arg := certificate a in
            let c2: f arg <> None := guarantee arg c1 in
            unsome (f arg) c2.

Ltac somify :=
  try unfold restrict;
  match goal with
  | [ |- context[Some (unsome _ _ )]] => rewrite some_unsome
  | [ |- unsome _ _ = unsome _ _ ] => apply somify_helper; rewrite some_unsome; rewrite some_unsome
  | [ |- unsome _ _ = _ ] => apply somify_helper; rewrite some_unsome
  | [ |- _ = unsome _ _ ] => apply somify_helper; rewrite some_unsome
  | [ |- _ ] => apply somify_helper
  end.

Lemma unsome_irrel {A: Type}
                   (o: option A) (p1 p2: o <> None):
  unsome o p1 = unsome o p2.
somify. trivial.
Qed.

Lemma unsome_eq {A: Type}
                (o1 o2: option A):
  o1 = o2 ->
    forall c1 c2, 
      unsome o1 c1 = unsome o2 c2.
intros.
somify. exact H.
Qed.

Lemma some_restrict {A B: Type} {P: A -> Prop}
                    (f: A -> option B)
                    (guarantee: forall x: A, P x -> f x <> None)
                    (a: {x: A | P x}):
  Some (restrict f guarantee a) = f (unspecify a).
somify. trivial. Qed.


Lemma some_restrict_intro {A B: Type} {P: A -> Prop}
                    (f: A -> option B)
                    (guarantee: forall x: A, P x -> f x <> None)
                    (a: A)
                    (ok: P a):
   f a = Some (restrict f guarantee (specify a ok)).
somify. trivial. Qed.


(*
 * Put unsome inside a predicate.
 *)
Lemma unsome_p {A} {P: A -> Prop} (o: option A)
               (ok: o <> None)
               (p: match o with 
                   | Some x => P x
                   | None => True
                   end)
: P (unsome o ok).
Proof.
assert(o = Some (unsome o ok)).
{
  somify. trivial.
}
rewrite H in p. exact p.
Qed.

Lemma unsome_p_back {A} {P: A -> Prop} (o: option A):
  forall ok,
    P (unsome o ok) -> 
        match o with 
        | Some x => P x
        | None => False
        end.
Proof.
intro. intro. assert(su: Some(unsome o ok) = o). somify. trivial.
rewrite<- su. exact H.
Qed.

(*
 * Put restrict inside a predicate.
 *)
Lemma restrict_p1 {A B} {InDomain: A -> Prop} {P: B -> Prop}
   (f: A -> option B)
   (guarantee: forall x: A, InDomain x -> f x <> None)
   (x: A)
   (x_ok: InDomain x)
   (p: match f x with
       | Some y => P y
       | None => True
       end)
: P ((restrict f guarantee) (specify x x_ok)).
Proof.
unfold restrict. apply unsome_p. simpl. exact p. 
Qed.

Lemma restrict_p2 {A B} {InDomain: A -> Prop} {P: B -> Prop}
   (f: A -> option B)
   (guarantee: forall x: A, InDomain x -> f x <> None)
   (x: {x: A | InDomain x})
   (p: match f (unspecify x) with
       | Some y => P y
       | None => True
       end)
: P ((restrict f guarantee) x).
Proof.
unfold restrict. apply unsome_p. exact p. Qed.

Lemma unrestrict_p {A B} {InDomain: A -> Prop} {P: B -> Prop}
     (f: A -> option B)
     (guarantee: forall x: A, InDomain x -> f x <> None)
     (x: {x: A | InDomain x})
: P ((restrict f guarantee) x) ->
   match f (unspecify x) with
   | Some y => P y
   | None => False
   end.
Proof.
assert(sr: Some (restrict f guarantee x) = f (unspecify x)). apply some_restrict.
rewrite<- sr. trivial. Qed.

Lemma restrict_p {A B} {InDomain: A -> Prop} {P: B -> Prop}
                 (f: A -> option B)
                 (guarantee: forall x: A, InDomain x -> f x <> None)
                 (none_part: Prop)
                 (x: {x: A | InDomain x})
: P ((restrict f guarantee) x) <->
   match f (unspecify x) with
   | Some y => P y
   | None => none_part
   end.
Proof.
split. 
{
  intro H. apply unrestrict_p in H. destruct (f (unspecify x)). trivial. contradiction.
}
intro H. apply restrict_p2. destruct (f (unspecify x)). trivial. trivial.
Qed.

Lemma restrict_compose {A B C} {InDomain: A -> Prop} {F: B -> C}
                       (f: A -> option B)
                       (guarantee: forall x: A, InDomain x -> f x <> None)
                       (none_part: C)
                       (x: {x: A | InDomain x})
: F ((restrict f guarantee) x) =
   match f (unspecify x) with
   | Some y => F y
   | None => none_part
   end.
Proof.
destruct x as (y, c).
simpl.
unfold restrict. simpl.
generalize (guarantee y c) as g. intro.
unfold unsome.
remember (f y) as z. simpl.
destruct z. trivial. exfalso. apply g. trivial.
Qed.

(* vim: set ft=coq: *)
