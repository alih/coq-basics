(* 
 * I have to do this shit because Coq's minmax seems to be unusable.
 *)

Require Import Omega.
Require Import Min.
Require Import Max.

Lemma leb_S:
  forall a b,
    leb (S a) (S b) = leb a b.
simpl. trivial. Qed.

(* I wonder why didn't they do that in the first place? *)
Definition min' (a b: nat) := if leb a b then a else b.
Definition max' (a b: nat) := if leb a b then b else a.

Lemma min'same:
  forall a,
    min' a a = a.
intros. unfold min'. destruct (leb a a). trivial. trivial. Qed.

Lemma max'same:
  forall a,
    max' a a = a.
intros. unfold max'. destruct (leb a a). trivial. trivial. Qed.

Lemma min'le:
  forall a b: nat,
    a <= b -> min' a b = a.
unfold min'. intros. assert(leb a b = true). apply leb_correct. assumption.
rewrite H0. trivial. Qed.

Lemma min'gt:
  forall a b: nat,
    a > b -> min' a b = b.
unfold min'. intros. assert(leb a b = false). apply leb_correct_conv. assumption.
rewrite H0. trivial. Qed.

Lemma min'lt:
  forall a b: nat,
    a < b -> min' a b = a.
intros.
assert(a <= b).
auto with arith. apply min'le. assumption. Qed.

Lemma min'ge:
  forall a b: nat,
    a >= b -> min' a b = b.
intros.
assert(a = b \/ a > b). omega.
destruct H0. rewrite H0. apply min'same. apply min'gt. assumption. Qed.

Lemma max'le:
  forall a b: nat,
    a <= b -> max' a b = b.
intros. assert(leb a b = true). apply leb_correct. assumption.
unfold max'. rewrite H0. trivial. Qed.

Lemma max'gt:
  forall a b: nat,
    a > b -> max' a b = a.
unfold max'. intros. assert(leb a b = false). apply leb_correct_conv. assumption.
rewrite H0. trivial. Qed.

Lemma max'lt:
  forall a b: nat,
    a < b -> max' a b = b.
intros.
assert(a <= b).
auto with arith. apply max'le. assumption. Qed.

Lemma max'ge:
  forall a b: nat,
    a >= b -> max' a b = a.
intros.
assert(a = b \/ a > b). omega.
destruct H0. rewrite H0. apply max'same. apply max'gt. assumption. Qed.

Lemma min_alt:
  forall a b,
    min a b = min' a b.
induction a. simpl. auto.
induction b. simpl. auto.
simpl. rewrite IHa. unfold min'. simpl. destruct (leb a b).
trivial. trivial. Qed.

Lemma max_alt:
  forall a b,
    max a b = max' a b.
induction a. simpl. auto.
induction b. simpl. auto.
simpl. rewrite IHa. unfold max'. simpl. destruct (leb a b).
trivial. trivial. Qed.

Lemma min_le (a b: nat): a <= b -> min a b = a.
rewrite min_alt. apply min'le. Qed.

Lemma min_lt (a b: nat): a < b -> min a b = a.
rewrite min_alt. apply min'lt. Qed.

Lemma min_ge (a b: nat): a >= b -> min a b = b.
rewrite min_alt. apply min'ge. Qed.

Lemma min_gt (a b: nat): a > b -> min a b = b.
rewrite min_alt. apply min'gt. Qed.

Lemma max_le (a b: nat): a <= b -> max a b = b.
rewrite max_alt. apply max'le. Qed.

Lemma max_lt (a b: nat): a < b -> max a b = b.
rewrite max_alt. apply max'lt. Qed.

Lemma max_ge (a b: nat): a >= b -> max a b = a.
rewrite max_alt. apply max'ge. Qed.

Lemma max_gt (a b: nat): a > b -> max a b = a.
rewrite max_alt. apply max'gt. Qed.

(**** we're completely back from min' and max' *****)

(* the lemmas are similar to stuff from http://phd.ia0.fr/coq/minmax.html *)

Lemma le_min: forall m n n', n <= n' -> min m n <= n'.
intros. assert(HH : (m <= n \/ n < m)). apply le_or_lt. destruct HH.
rewrite min_le. omega. assumption. rewrite min_gt. omega. assumption. Qed.

Lemma lt_min: forall m n n', n < n' -> min m n < n'.
intros. assert(HH : (m <= n \/ n < m)). apply le_or_lt. destruct HH.
rewrite min_le. omega. assumption. rewrite min_gt. omega. assumption. Qed.

Lemma le_max: forall m n n', n' <= n -> n' <= max m n.
intros. assert(HH : (m <= n \/ n < m)). apply le_or_lt. destruct HH.
rewrite max_le.  omega. assumption. rewrite max_gt. omega. assumption. Qed.

Lemma lt_max: forall m n n', n' < n -> n' < max m n.
intros. assert(HH : (m <= n \/ n < m)). apply le_or_lt. destruct HH.
rewrite max_le.  omega. assumption. rewrite max_gt. omega. assumption. Qed.

Lemma gt_min: forall n m p : nat, p < n -> p < m -> p < min n m.
intros. rewrite min_alt. unfold min'. destruct leb.
assumption. assumption. Qed.

Lemma gt_max: forall n m p : nat, n < p -> m < p -> max n m < p.
intros. rewrite max_alt. unfold max'. destruct leb.
assumption. assumption. Qed.

Ltac kick_min := 
  repeat(apply min_glb || apply le_n_S || apply gt_min
    || apply le_min || apply lt_min 
    || apply min_le || apply min_lt || apply min_ge || apply min_gt).

Ltac kick_max :=
  repeat(apply max_lub || apply le_n_S || apply gt_max
    || apply le_max || apply lt_max
    || apply max_le || apply max_lt || apply max_ge || apply max_gt).
