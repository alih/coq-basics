Require Import Coq.Lists.List.
Require Import Coq.Arith.Lt.
Require Basics.List.

Definition HasDup {A} (l: list A) := 
  exists (a b c: list A) (x: A),
    l  =  a  ++  x :: b  ++  x :: c. 

Theorem nodup_equiv {A} (l: list A):
    NoDup l <-> ~ HasDup l.
Proof.
split.
{ unfold not. intros. 
unfold HasDup in H0. destruct H0. destruct H0. destruct H0. destruct H0.
assert(~In x2 (x ++ x0 ++ x2 :: x1)). apply NoDup_remove_2. rewrite<- H0. assumption.
apply H1. rewrite app_assoc. apply in_or_app. right. auto with *.
}

intros. induction l. constructor. constructor. intro. apply H.
unfold HasDup.
assert(exists b, exists c, l = b ++ a :: c).
apply in_split. assumption. destruct H1. destruct H1.
rewrite H1. exists nil. exists x. exists x0. exists a.
simpl. trivial. apply IHl. unfold not. intros.
apply H. unfold HasDup in H0. destruct H0. destruct H0. destruct H0. destruct H0.
unfold HasDup.
exists (a :: x). exists x0. exists x1. exists x2.
rewrite H0. simpl. trivial. Qed.

Theorem hasdup_cons {A} (x: A) (l: list A):
    HasDup l -> HasDup (x :: l).
Proof.
unfold HasDup.
intros. destruct H. destruct H. destruct H. destruct H.
exists (x :: x0). exists x1. exists x2. exists x3. rewrite H. simpl. trivial.
Qed.

Lemma remove_preserves_in {A}
    (eq_dec : forall x y: A, {x = y}+{x <> y})
    (x y: A) (l: list A):
  In x (remove eq_dec y l) <-> x <> y /\ In x l.
Proof.
induction l. { simpl. tauto. }
simpl. destruct eq_dec as [y_eq_a | y_ne_a].
{
  rewrite IHl.
  rewrite y_eq_a.
  split. { tauto. }
  intros (x_ne_a, H).
  split. { assumption. }
  destruct H. { symmetry in H. contradiction. }
  assumption.
}
simpl. rewrite IHl. split. 
{
  intro H.
  destruct H.
  {
    rewrite H.
    split.
    {
      rewrite H in y_ne_a.
      intro F. symmetry in F. contradiction.
    }
    left. trivial.
  }
  destruct H as (x_ne_y, In_x_l).
  split. { assumption. }
  right. assumption.
}
intros (x_ne_y, H).
destruct H as [a_eq_x | In_x_l].
{
  left. assumption.
}
right. split. assumption. assumption.
Qed.

Lemma remove_length_le {A}
    (eq_dec : forall x y: A, {x = y}+{x <> y})
    (x: A) (l: list A):
  length (remove eq_dec x l) <= length l.
Proof.
induction l.
{
  simpl. trivial.
}
simpl. destruct eq_dec. auto with arith.
simpl. auto with arith.
Qed.

Lemma remove_length_lt {A}
    (eq_dec : forall x y: A, {x = y}+{x <> y})
    (x: A) (l: list A):
  In x l -> length (remove eq_dec x l) < length l.
Proof.
induction l.
{
  simpl. tauto.
}
simpl.
intro H.
destruct H.
{
  rewrite H. destruct eq_dec.
  {
    assert(LE := remove_length_le eq_dec x l).
    auto with arith.
  }
  simpl.
  exfalso.
  apply n. trivial.
}
apply IHl in H.
destruct eq_dec.
auto with arith.
simpl. auto with arith.
Qed.


Theorem dirichlet {A} (a b: list A)
    (eq_dec : forall x y: A, {x = y}+{x <> y}):
  (forall x, In x a -> In x b) ->
    length a > length b ->
      HasDup a.
Proof.
generalize a b.
induction a0.
{
  intros b0 H F. simpl in F. exfalso. 
  assert (~(0 > length b0)). auto with arith.
  tauto.
}
intros b0 H Len.
assert(In a0 b0) as a0b0.
{
  apply H. simpl. left. trivial.
}
remember (remove eq_dec a0 a1) as a'.
remember (remove eq_dec a0 b0) as b'.
assert(length b' < length b0) as b_descent.
{
  rewrite Heqb'. apply remove_length_lt. exact a0b0.
} 
assert(a0a1_dec: {In a0 a1} + {~In a0 a1}).
{
   apply in_dec. apply eq_dec.
}
destruct a0a1_dec.
{
  rewrite Basics.List.in_decomp in i.
  destruct i as (ia, (ib, i)). rewrite i.
  exists nil. exists ia. exists ib. exists a0. simpl. trivial.
}
assert(forall x : A, In x a1 -> In x b0) as a1_subset_b0.
{
  intros.
  apply H. simpl. right. assumption.
}
assert(forall x : A, In x a1 -> In x b') as a1_subset_b'.
{
  intros. rewrite Heqb'.
  assert(x <> a0).
  {
    intro oops. rewrite oops in H0. tauto.
  }
  apply remove_preserves_in. split. { assumption. }
  apply a1_subset_b0. assumption.
}
assert (length a1 > length b') as Len_ok.
{
  simpl in Len.
  assert(length a1 >= length b0). { auto with arith. }
  apply Lt.lt_le_trans with (m:=length b0). { assumption. }
  auto with arith.
}
assert(HasDup a1).
{
  apply IHa0 with (b:=b'). assumption. assumption.
}
apply hasdup_cons. assumption.
Qed.
