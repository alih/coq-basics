Require Import Lists.List.
Require Import Basics.DepTypes.
Require Import Basics.List.
Require Import Basics.ListIndex.

Definition forallb {A: Type} (p: A -> bool) (l: list A): bool
:= fold_right andb true (map p l).

Fixpoint forallb' {A: Type} (p: A -> bool) (l: list A): bool
:= match l with
   | cons head tail => andb (p head) (forallb' p tail)
   | nil => true
   end.

Lemma forallb_alt {A: Type} (p: A -> bool) (l: list A):
  forallb p l = forallb' p l.
induction l. simpl. trivial. simpl. rewrite<- IHl.
unfold forallb. simpl. trivial. Qed.

Lemma Forall_forallb {A: Type} (p: A -> bool) (l: list A):
  Forall (fun x => p x = true) l <-> forallb p l = true.
split.
{
  intro F.
  rewrite forallb_alt.
  induction l.
  {
    simpl. trivial.
  }
  simpl. apply andb_true_intro. split.
  {
    rewrite Forall_forall in F. simpl in F. apply F.
    left. trivial.
  }
  apply IHl. rewrite Forall_forall in F. simpl in F. 
  rewrite Forall_forall. intro.
  intro is_in. apply F. right. assumption.
}
rewrite forallb_alt. rewrite Forall_forall.
induction l.
{
  simpl. tauto.
}
intro fb. simpl. simpl in fb. apply andb_prop in fb. 
destruct fb as (fba, fbl). intro x.
intro a_or_l.
destruct a_or_l.
{
  rewrite<- H. assumption.
}
apply IHl. assumption. assumption.
Qed. 

Definition existsb {A: Type} (p: A -> bool) (l: list A): bool
:= fold_right orb false (map p l).

Fixpoint existsb' {A: Type} (p: A -> bool) (l: list A)
:= match l with
   | cons head tail => orb (p head) (existsb' p tail)
   | nil => false
   end.

Lemma existsb_alt {A: Type} (p: A -> bool) (l: list A):
  existsb p l = existsb' p l.
induction l. simpl. trivial. simpl. rewrite<- IHl.
unfold forallb. simpl. trivial. Qed.

Lemma Exists_existsb {A: Type} (p: A -> bool) (l: list A):
  Exists (fun x => p x = true) l <-> existsb p l = true.
rewrite Exists_exists. rewrite existsb_alt.
split.
{
  induction l.
  {
    simpl.
    intro.
    destruct H. tauto.
  }
  simpl.
  intro Ex.
  destruct Ex as (x, Ex).
  destruct Ex as (a_or_l, px).
  destruct a_or_l.
  {
    subst a.
    rewrite px.
    simpl. trivial.
  }
  assert(existsb' p l = true).
  {
    apply IHl. exists x.
    tauto.
  }
  rewrite H0.
  auto with *.
}
induction l.
{
  simpl. intro. inversion H. 
}
simpl. intro Or. apply Bool.orb_prop in Or.
destruct Or.
{
  exists a. tauto.
}
apply IHl in H.
destruct H.
exists x.
tauto.
Qed.

Lemma bound_S_helper (a: nat) (b: nat): a < b -> S a < S b.
auto with arith. Qed.

Definition bound_S {A: Type} (b: nat) (a: {x | x < b}) : {x | x < S b} :=
  specify (S (unspecify a)) (bound_S_helper (unspecify a) b (certificate a)).

Lemma ith_S_helper {A: Type} {l: list A} {a: A}  
                   (x: nat) : x < length l -> S x < length (a :: l).
simpl. auto with arith. Qed.

Definition ith_S {A: Type} {a: A}
                 (l: list A)
                 (i: {x | x < length l})
: {x | x < length (a :: l)}
:= specify (S (unspecify i)) (ith_S_helper (unspecify i) (certificate i)).

Lemma ith_S_cons {A: Type}
                 (l: list A)
                 (i: {x | x < length l})
                 (a: A):
    ith (cons a l) (ith_S l i) = ith l i.
Proof.
rewrite ith_nth_self. simpl. rewrite<- ith_nth. trivial.
Qed.

Lemma ith_forall_ltr {A: Type} 
                     (l: list A)
                     (P: A -> Prop):
  (forall i, P (ith l i)) -> Forall P l.
rewrite Forall_forall.
intro all_P_ith.
intro x.
intro x_in.
assert(forall i : {x : nat | x < length l}, match nth_error l (unspecify i) with
                                            | Some a => P a
                                            | None => False
                                            end).
{
  intro i.
  apply (unrestrict_p (nth_error l) (nth_error_works l) i).
  exact (all_P_ith i).
}
clear all_P_ith.
induction l.
{
  simpl in x_in. contradiction.
}
simpl in x_in. destruct x_in as [a_is_x | x_in_l].
{
  subst.
  assert(c: O < length(x :: l)).
  {
    simpl. auto with arith.
  }
  remember (exist (fun z => z < length(x :: l)) 0 c) as zero.
  assert(Hx := H zero).
  rewrite Heqzero in Hx.
  simpl in Hx.
  assumption.
}
apply IHl in x_in_l.
assumption. clear IHl.
intro.
remember (unspecify i) as j.
assert(j_ok: j < length l). rewrite Heqj. exact (certificate i).
assert (sj_ok: S j < S (length l)). auto with arith.
assert(Hj:= H (specify (S j) sj_ok)). clear H.
simpl in Hj. exact Hj.
Qed.

Lemma ith_forall_rtl {A: Type} (l: list A) (P: A -> Prop):
  Forall P l -> forall i, P (ith l i).
Proof.
rewrite Forall_forall.
intro. intro i.
apply H. apply ith_In.
Qed.

Theorem ith_forall {A: Type} 
                   (l: list A)
                   (P: A -> Prop):
  (forall i, P (ith l i)) <-> Forall P l.
Proof.
split.
apply ith_forall_ltr. apply ith_forall_rtl.
Qed.

(*********************************************************************)
Lemma Forall_head {A: Type} {P: A -> Prop}
                          (h: A) (t: list A)
                          (good: Forall P (cons h t)):
  P h.
rewrite Forall_forall in good.
simpl in good.
apply good.
left. trivial.
Qed.

Lemma Forall_tail {A: Type} {P: A -> Prop}
                          (h: A) (t: list A)
                          (good: Forall P (cons h t)):
  Forall P t.
rewrite Forall_forall in good.
rewrite Forall_forall.
simpl in good.
firstorder.
Qed.

Lemma Forall_app {A} (a b: list A) (P: A -> Prop):
    Forall P (a ++ b) <-> Forall P a /\ Forall P b.
Proof.
repeat rewrite Forall_forall.
split; intro H.
split;
   intros x Xin; assert (Hx := H x);
   rewrite in_app_iff in Hx; tauto. 
intros x I. rewrite in_app_iff in I.
firstorder.
Qed.

Program Fixpoint mapgood {A B: Type} {P: A -> Prop}
                 (f: forall x: A, P x -> B)
                 (l: list A) (good: Forall P l)
: list B
:= match l with
   | nil => nil
   | h :: t => (f h (Forall_head h t good)) 
                    :: mapgood f t (Forall_tail h t good)
   end.

Lemma mapgood_length {A B: Type} {P: A -> Prop}
                 (f: forall x: A, P x -> B)
                 (l: list A) (good: Forall P l):
  length (mapgood f l good) = length l.
induction l.
{ simpl. trivial. }
simpl. rewrite IHl. trivial.
Qed.

Program Fixpoint map_specify {A: Type} {P: A -> Prop}
                             (l: list A)
                             (good: Forall P l)
: list {x: A | P x}
:= match l with 
   | nil => nil
   | h :: t => (specify h (Forall_head h t good)) 
                :: 
               map_specify t (Forall_tail h t _)
   end.

Lemma map_specify_length {A: Type} {P: A -> Prop}
                         (l: list A)
                         (good: Forall P l):
  length (map_specify l good) = length l.
Proof.
induction l.
- simpl. trivial.
- simpl. rewrite<- IHl with (good := Forall_tail a l good).
  trivial.
Qed.

Lemma map_unspecify_map_specify
    {A} {P: A -> Prop}
    (l: list A) (G: Forall P l):
  map unspecify (map_specify l G) = l.
Proof.
induction l. { simpl. trivial. }
simpl. rewrite IHl. trivial.
Qed.

Definition spread {A: Type} {P: A -> Prop}
                  (l: {x: list A | Forall P x})
: list {x: A | P x}
:= map_specify (unspecify l) (certificate l).

Lemma deplist_forall {A: Type} {P: A -> Prop}
                     (l: list {x: A | P x})
: Forall P (map unspecify l).
Proof.
induction l.
{ simpl. trivial. }
simpl. apply Forall_cons.
exact (certificate a).
assumption.
Qed.

Definition gather {A: Type} {P: A -> Prop}
                  (l: list {x: A | P x})
: {x: list A | Forall P x}
:= specify (map unspecify l) (deplist_forall l).
