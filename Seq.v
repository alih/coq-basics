Require Import Lists.List.

(** ** Seq0 and seq *)

(** The 0...(n-1) sequence. *)
Definition seq0 (n:nat) := seq 0 n.

(** Items of a sequence are bound from below by its start. *)
Theorem seq_lower_bound:
    forall start len i: nat,
        In i (seq start len) -> i >= start.
induction start.
(* base *)
intros.
auto with *.
(* step *)
intros.
assert (In i (map S (seq start len))).
rewrite seq_shift. exact H.
assert (exists j, S j = i /\ In j (seq start len)).
apply in_map_iff. exact H0.
destruct H1.
destruct H1.
rewrite<- H1.
assert (x >= start).
apply IHstart with len. exact H2.
auto with *. Qed.

(** Head of a sequence. *)
Theorem seq_head:
    forall start len head tail,
        head :: tail = seq start len -> head = start.
destruct len. intros. discriminate H.
intros. injection H. intros. exact H1. Qed.

(** Tail of a sequence. *)
Theorem seq_tail:
    forall start len head tail,
        head :: tail = seq start len -> tail = seq (S start) (len - 1).
destruct len. intros. discriminate H.
intros. injection H. intros.
assert (S len - 1 = len).
unfold minus. fold minus.
auto with *.
rewrite H2. exact H0. Qed. 

(** Head of a seq0 is 0. *)
Theorem seq0_head:
    forall n head tail,
        head :: tail = seq0 n -> head = 0.
unfold seq0.
apply seq_head. Qed.

(** Tail of a iota through map S. *)
Theorem seq0_tail:
    forall n head tail,
        head :: tail = seq0 n -> tail = map S (seq0 (n - 1)).
unfold seq0.
intros.
assert (tail = seq 1 (n - 1)). apply seq_tail with head. exact H.
rewrite seq_shift. exact H0. Qed.

(** Length of a seq0 *)
Theorem seq0_length:
    forall n,
        length (seq0 n) = n.
unfold seq0.
intros.
apply seq_length. Qed.

(** Indexing seq0 with a range check. *)
Theorem seq0_nth_in_range:
    forall n len d,
        n < len -> nth n (seq0 len) d = n.
unfold seq0. intros. apply seq_nth. exact H. Qed.

(** Indexing seq0 with a default. *)
Theorem seq0_nth:
    forall n len,
        nth n (seq0 len) n = n.
intros.
assert(len <= n \/ n < len).
apply Lt.le_or_lt.
destruct H.
apply nth_overflow.
rewrite seq0_length. assumption.
apply seq0_nth_in_range. assumption. Qed.

(** Upper bound on the iota items.
 This theorem works both ways.
 *)
Theorem seq0_upper_bound:
    forall n i: nat,
        In i (seq0 n) <-> i < n.
Proof.
        assert (forall n i: nat,
                In i (seq0 n) -> i < n) as seq0_upper_bound_right.
          unfold seq0.
          induction n.
          unfold seq. unfold In. contradiction.
          unfold seq. fold seq. rewrite<- seq_shift. intros.
          destruct H. rewrite <- H.
          auto with *.
          assert (exists j, S j = i /\ In j (seq 0 n)).
          apply in_map_iff. apply H. destruct H0. destruct H0.
          rewrite<- H0.
          assert (x < n).
          apply IHn. exact H1.
          auto with *.


        assert(forall n i: nat,
                i < n -> In i (seq0 n)) as seq0_upper_bound_left.
          intros.
          rewrite<- seq0_nth with (len:=n) (n:=i).
          apply nth_In.
          rewrite seq0_length.
          assumption.

intros.
split.
apply seq0_upper_bound_right.
apply seq0_upper_bound_left.
Qed.

(** Upper bound of a sequence. *)
Lemma seq_upper_bound:
    forall start len i: nat,
        In i (seq start len) -> i < start + len.
induction start.
apply seq0_upper_bound.
intros.
rewrite<- seq_shift in H. apply in_map_iff in H.
destruct H. destruct H.
apply IHstart in H0.
rewrite <- H.
assert (S start + len = S (start + len)).
trivial.
rewrite H1.
auto with *.
Qed. 

(* vim: set ft=coq: *)
