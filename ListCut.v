(* ListCut.v - theorems about firstn and skipn
 * (C) 2015  Ilya Mezhirov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *)


Require Import Coq.Lists.List.
Require Import PeanoNat.
Require Import Basics.List.
Require Import Basics.DepTypes.
Require Import Basics.MinMax.

Section Cutting.

Variable A: Type.

Fixpoint firstn' (n:nat)(l:list A){struct l} : list A :=
  match l with
  | nil => nil
  | h :: t => match n with
              | 0 => nil
              | S k => h :: (firstn k t)
              end
  end.

Lemma firstn_alt:
  forall n l,
    firstn n l = firstn' n l.
induction n.
induction l.
simpl. trivial.
simpl. trivial.
simpl. intros.
induction l. trivial. trivial. Qed.

Fixpoint skipn' (n:nat)(l:list A){struct l} : list A :=
  match l with
  | nil => nil
  | h :: t => match n with
              | 0 => h :: t
              | S k => skipn' k t
              end
  end.

Lemma skipn_alt:
  forall n l,
    skipn n l = skipn' n l.
Proof.
induction n. 
{ induction l; easy. }
simpl. intros.
destruct l. { easy. } simpl. exact (IHn l). 
Qed.

Lemma firstn_overflow:
  forall (l: list A) n,
    n >= length l -> firstn n l = l.
Proof.
induction l. 
{ simpl. intro. now rewrite firstn_alt. }
intro. rewrite firstn_alt. simpl. intro.
destruct n.
{ apply Nat.nle_succ_0 in H. contradiction. }
rewrite IHl. trivial. auto with arith. 
Qed. 

Lemma firstn_firstn:
  forall (l garbage: list A) n,
    n < length l ->
      firstn n (firstn n l ++ garbage) = firstn n l.
Proof.
induction l.
{ simpl. intros. apply Nat.nlt_0_r in H. contradiction. }
intros. rewrite firstn_alt. rewrite firstn_alt. simpl.
induction n. simpl. rewrite<- firstn_alt. simpl. trivial.
rewrite<- firstn_alt. simpl. 

assert(firstn n (firstn n l ++ garbage) = firstn n l).
apply IHl. simpl in H. auto with arith. rewrite H0. trivial.
Qed.

Lemma skipn_app:
  forall (l l': list A),
    skipn (length l) (l ++ l') = l'.
induction l. simpl. trivial.
simpl. assumption. Qed.

Lemma skipn_length:
  forall (l: list A) n,
    n + length(skipn n l) = max n (length l).
Proof.
induction l.
{ 
  simpl. intro. rewrite skipn_alt. simpl.
  induction n. simpl. trivial. simpl.
  auto with arith.
}
intros. rewrite skipn_alt. simpl.
induction n. simpl. trivial.
rewrite<- skipn_alt. 
assert(S n + length (skipn n l) = S(n + length (skipn n l))).
auto with arith.
rewrite H. rewrite IHl. simpl. trivial.
Qed.

Lemma firstn_nth:
  forall a (n k: nat) (d: A),
    k < n ->
      nth k a d = nth k (firstn n a) d.
intros.
assert(k >= length a \/ k < length a).
apply Lt.le_or_lt.
destruct H0.
assert(nth k a d = d).
apply(nth_overflow). auto with arith.
assert(nth k (firstn n a) d = d).
apply(nth_overflow).  rewrite firstn_length.
kick_min. auto with arith. rewrite H1. symmetry. exact H2.
assert((firstn n a) ++ (skipn n a) = a).
apply(firstn_skipn).
rewrite<- H1 at 1.
apply(app_nth1).
rewrite(firstn_length).
kick_min. auto with arith. auto with arith. Qed.

Lemma skipn_decompose:
    forall a (n: nat) (d: A),
      n < length a ->
        skipn n a = (nth n a d) :: (skipn (S n) a).
Proof.
induction a.
simpl. intros.  apply Nat.nlt_0_r in H. contradiction. 
simpl. intros. destruct n. simpl. trivial.
simpl. assert(n < length a0). auto with arith.
apply IHa with (d:=d) in H0.
rewrite H0. simpl. trivial.
Qed.

Lemma firstn_nil (n: nat):
  firstn n (@nil A) = nil.
Proof.
induction n; easy.
Qed.

Lemma skipn_nil (n: nat):
  skipn n (@nil A) = nil.
Proof.
induction n; easy.
Qed.

Lemma firstn_In_S (l: list A) (n: nat)
                  (x: A) (H: In x (firstn n l)):
  In x (firstn (S n) l).
Proof.
generalize n H. clear n H.
induction l; intros. { rewrite firstn_nil in H. easy. }
cbn. destruct n. { easy. }
destruct H. tauto. right. apply IHl. assumption.
Qed.

Lemma firstn_In (l: list A) (n: nat)
                (x: A) (H: In x (firstn n l)):
  In x l.
Proof.
induction l. { rewrite firstn_nil in H. assumption. }
rewrite firstn_alt in H. cbn in H.
destruct n. { easy. }
cbn in *. destruct l; try easy. rewrite firstn_nil in H. tauto.
destruct H; try tauto. right. apply IHl. clear IHl.
destruct n. { easy. }
cbn in H. destruct H. cbn. tauto.
apply firstn_In_S in H.
cbn in *. right. assumption.
Qed.

Lemma Forall_firstn {P: A -> Prop} (l: list A) (n: nat)
                    (H: Forall P l):
  Forall P (firstn n l).
Proof.
rewrite Forall_forall in *. intros x Hf. apply H.
apply (firstn_In _ _ _ Hf).
Qed.

Lemma map_firstn {B} (l: list A) (f: A -> B) (n: nat):
  map f (firstn n l) = firstn n (map f l).
Proof.
generalize n. clear n.
induction l; intros. cbn.
rewrite firstn_nil. induction n; easy.
cbn. destruct n. { easy. }
cbn. f_equal. apply IHl.
Qed.

Lemma ith_skipn_decomp (l: list A) (f: A -> nat)
                     (i: { x: nat | x < length l}):
  skipn (unspecify i) l = ith l i :: skipn (S (unspecify i)) l.
Proof.
assert(c := certificate i). remember (unspecify i) as n. cbn in c.
rewrite skipn_decompose with (d := ith l i); try assumption.
f_equal. subst. rewrite<- ith_nth_self. trivial.
Qed.

Lemma ith_decomp (l: list A) (f: A -> nat)
                 (i: { x: nat | x < length l}):
  l = firstn (unspecify i) l ++ ith l i :: skipn (S (unspecify i)) l.
Proof.
rewrite<- ith_skipn_decomp. rewrite firstn_skipn. trivial.
exact f. (* wtf *)
Qed.


End Cutting.

(* vim: set ft=coq: *)
